@extends('layouts.bootstrap')

@section('title')
  Главная страница
@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div style="text-align: right; float: right;"><a href="/mainpage/edit">Редактировать</a></div>
      <h1>{{ $mainpage->main_page_text }}</h1>
    </div>
  </div>
</div>
@stop
