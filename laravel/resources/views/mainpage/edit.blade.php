@extends('layouts.bootstrap')

@section('title')
  Редактировать главную страницу
@stop

@section('content')
<div class="row">
  {!! Form::model($mainpage, ['method' => 'PATCH', 'action' => ['MainPageController@update']]) !!}
    <div class="form-group">
      {!! Form::label('main_page_text', 'Текст на главной' ) !!}
      {!! Form::textarea('main_page_text', null, ['class' => 'form-control', 'rows' => 5] ) !!}
    </div>
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary'] ) !!}
  {!! Form::close() !!}
</div>

<div class="row">
  @include('includes.form_error')
</div>
@stop
