@extends('layouts.bootstrap')

@section('title')
  Создать новость
@stop


@section('content')
  <div class="row">
    <h1>Создать новость</h1>
  </div>
  <div class="row">
    {!! Form::open(['method' => 'POST', 'action' => 'NewsController@store']) !!}
      <div class="form-group">
        {!! Form::label('header', 'Заголовок' ) !!}
        {!! Form::text('header', null, ['class' => 'form-control'] ) !!}
      </div>
      <div class="form-group">
        {!! Form::label('content', 'Текст новости' ) !!}
        {!! Form::textarea('content', null, ['class' => 'form-control'] ) !!}
      </div>
      <div class="form-group">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary'] ) !!}
      </div>
    {!! Form::close() !!}
  </div>
  <div class="row">
    @include('includes.form_error')
  </div>
@stop
