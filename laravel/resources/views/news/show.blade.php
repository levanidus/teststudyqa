@extends('layouts.bootstrap')

@section('title')
  Просмотр новости {{ $onenew->header }}
@stop


@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div style="text-align: right; float: right;"><a href="{{ route('news.edit', $onenew->id)}}">Редактировать новость</a></div>
      <h1>{{ $onenew->header }}</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <em>{!! nl2br(e($onenew->content)) !!}</em>
    </div>
  </div>
@stop
