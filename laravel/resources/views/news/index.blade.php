@extends('layouts.bootstrap')

@section('title')
  Список новостей
@stop


@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div style="text-align: right; float: right;"><a href="{{ route('news.create') }}">Создать новость</a></div>
      <h1>Новости</h1>
      @if($news)
      @foreach($news as $onenew)
      <a href="{{ route('news.show', $onenew->id) }}"><h2>{{ $onenew->header }}</h2></a>
      @endforeach
    @endif
    </div>
  </div>
@stop
