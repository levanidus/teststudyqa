<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MainPageUpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'main_page_text' => 'required'
      ];
    }

    public function messages()
     {
         return [
             'main_page_text.required' => 'Заполните поле Текст на главной'
         ];
     }
}
