<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'header' => 'required',
            'content' => 'required'
        ];
    }

    public function messages()
    {
       return [
           'header.required' => 'Заполните поле Заголовок',
           'content.required' => 'Заполните поле Текст новости',
       ];
    }
}
