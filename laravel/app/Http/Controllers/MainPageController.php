<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\MainPageUpdateRequest;

use App\MainPage;

class MainPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $mainpage = MainPage::findOrFail(1);

       return view('mainpage.mainpage', compact('mainpage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
      $mainpage = MainPage::findOrFail(1);

      return view('mainpage.edit', compact('mainpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainPageUpdateRequest $request)
    {
      $input = $request->all();

      $mainpage = MainPage::findOrFail(1);

      $mainpage->update($input);

      return redirect('/');
    }

}
