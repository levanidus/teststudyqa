<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
    protected $fillable = ['main_page_text'];
}
